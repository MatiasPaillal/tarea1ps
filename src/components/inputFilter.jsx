function InputFilter(data) {
  return (
    <input
      placeholder={data.placeholder}
      className="p-2 rounded-md m-5"
      type="text"
      onChange={(e) => data.method(e.target.value)}
    />
  );
}

export default InputFilter;
