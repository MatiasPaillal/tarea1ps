import PropTypes from 'prop-types';
function SwitchComponent({ onClick }) {
    return (
        <>
            <label className="relative inline-flex cursor-pointer items-center">
                <input id="switch" type="checkbox" className="peer sr-only" />
                <label htmlFor="switch" className="hidden"></label>
                <div className="peer switchButton" onClick={onClick}></div>
            </label>
        </>
    )
}
SwitchComponent.propTypes = {
    onClick: PropTypes.func.isRequired
}

export default SwitchComponent;
