import AutoTableRow from "./AutoTableRow";
import PropTypes from 'prop-types';

function AutoTable({ automobiles, isAgente }) {
  if (automobiles.length === 0) {
    return <p>No hay automóviles disponibles.</p>;
  }

  const headers = Object.keys(automobiles[0]);

  return (
    <table className="automobilesTable">
      <thead>
        <tr>
          {
            isAgente ?
              headers.map((header) => (
                <th key={header} >{header}</th>
              ))
              :
              headers.map((header) => (
                header !== "popularidad" ?
                  <th key={header}>{header}</th>
                  :
                  null
              ))
          }
        </tr>
      </thead>
      <tbody>
        {automobiles.map((automobil, index) => (
          <AutoTableRow key={index} headers={headers} automobil={automobil} isAgente={isAgente} />
        ))}
      </tbody>
    </table>
  );
}

AutoTable.propTypes = {
  automobiles: PropTypes.array.isRequired,
  isAgente: PropTypes.bool.isRequired,
}

export default AutoTable;
