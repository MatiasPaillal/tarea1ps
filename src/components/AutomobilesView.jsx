import { useEffect, useState } from "react";
import { generateRandomObject } from "../utils/objectGenerator";
import AutoTable from "./AutoTable";
import InputFilter from "./InputFilter";
import SwitchComponent from "./SwitchComponent";

function AutomobilesView(data) {
  const [automobiles, setAutomobiles] = useState([]);
  const [filterType, setFilterType] = useState(null);
  const [filterColor, setFilterColor] = useState(null);
  const [filterPrice, setFilterPrice] = useState(null);
  const [isAgente, setIsAgente] = useState(false);

  useEffect(() => {
    console.log("automobileData");
  }, []);

  const generateAutomobil = () => {
    const newAutomobile = generateRandomObject(data.data);
    setAutomobiles([...automobiles, newAutomobile]);
  };
  const deleteAutomobiles = () => {
    setAutomobiles([]);
  };
  /**
   * Filtra los automóviles por precio si se proporciona un filtro de precio,
   * de lo contrario, devuelve todos los automóviles.
   *
   * @param {number} filterPrice - El precio máximo para el filtro.
   * @param {Array} automobiles - Un arreglo de objetos de automóviles.
   * @returns {Array} - Un arreglo de automóviles filtrados por precio o todos los automóviles si no hay filtro de precio.
   */
  const filterByPrice = filterPrice
    ? automobiles.filter((automobile) => {
        return automobile.precio <= filterPrice;
      })
    : automobiles;

  /**
   * Filtra los automóviles por color si se proporciona un filtro de color,
   * de lo contrario, mantiene los resultados anteriores.
   *
   * @param {string} filterColor - El color a filtrar (en minúsculas).
   * @param {Array} filterByPrice - Un arreglo de automóviles filtrados por precio.
   * @returns {Array} - Un arreglo de automóviles filtrados por color o los resultados anteriores si no hay filtro de color.
   */
  const filterByColor = filterColor
    ? filterByPrice.filter((automobile) => {
        return automobile.color
          .toLowerCase()
          .includes(filterColor.toLowerCase());
      })
    : filterByPrice;

  /**
   * Filtra los automóviles por tipo si se proporciona un filtro de tipo,
   * de lo contrario, mantiene los resultados anteriores.
   *
   * @param {string} filterType - El tipo de automóvil a filtrar (en minúsculas).
   * @param {Array} filterByColor - Un arreglo de automóviles filtrados por color.
   * @returns {Array} - Un arreglo de automóviles filtrados por tipo o los resultados anteriores si no hay filtro de tipo.
   */
  const filterByType = filterType
    ? filterByColor.filter((automobile) => {
        return automobile.tipo.toLowerCase().includes(filterType.toLowerCase());
      })
    : filterByColor;

  return (
    <>
      <button className="m-5" onClick={generateAutomobil}>
        Generar automoviles
      </button>
      <button className="m-5" onClick={deleteAutomobiles}>
        Eliminar todos los automoviles
      </button>
      <div className="flex justify-evenly items-center">
        <h1 className="text-xl">Filtrar por:</h1>
        <InputFilter placeholder={"Tipo"} method={setFilterType} />
        <InputFilter placeholder={"Color"} method={setFilterColor} />
        <InputFilter placeholder={"Precio"} method={setFilterPrice} />
        <div className="flex flex-col justify-start space-y-2 items-center">
          <h3>Modo Agente</h3>
          <SwitchComponent
            onClick={() => {
              setIsAgente(!isAgente);
            }}
          />
        </div>
      </div>
      <div className=" w-full m-5">
        <AutoTable automobiles={filterByType} isAgente={isAgente} />
      </div>
    </>
  );
}

export default AutomobilesView;
