import PropTypes from 'prop-types';
import usePopularidad from '../hooks/usePopularidad';

function AutoTableRow({ headers, automobil, isAgente }) {

    const [popularidad, aumentar] = usePopularidad(automobil.popularidad);

    automobil.popularidad = popularidad;

    return (
        <tr key={automobil.id} className='hover:bg-slate-600'>
            {headers.map((header) => (
                isAgente ?
                    <td key={header}>{automobil[header]}</td>
                    :
                    header !== "popularidad" ?
                        <td key={header}>{automobil[header]}</td>
                        :
                        null
            ))}
            {
                isAgente ?
                    null
                    :
                    <td><button className='text-sm px-2 py-1' onClick={aumentar}>Contactar agencia</button></td>
            }
        </tr>
    )
}

AutoTableRow.propTypes = {
    headers: PropTypes.array.isRequired,
    automobil: PropTypes.shape({
        id: PropTypes.string.isRequired,
        popularidad: PropTypes.number.isRequired,
    }).isRequired,
    isAgente: PropTypes.bool.isRequired,
}


export default AutoTableRow;