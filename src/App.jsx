import "./App.css";
import automobiles from "./data/automobiles.json";
import AutomobilesView from "./components/AutomobilesView";

function App() {
  return (
    <div className="w-full h-screen">
      <AutomobilesView data={automobiles} />
    </div>
  );
}

export default App;
