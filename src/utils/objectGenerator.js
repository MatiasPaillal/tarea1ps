/**
 * Genera un objeto aleatorio utilizando las especificaciones de datos proporcionadas.
 *
 * @param {Object} data - Un objeto que contiene las especificaciones de datos para generar el objeto.
 * @returns {Object} - Un objeto aleatorio creado según las especificaciones proporcionadas.
 */
export function generateRandomObject(data) {
  let randomObject = {};
  randomObject["id"] = generateUniqueId(); // Genera un ID único para el objeto

  for (const attributeType in data) {
    if (Array.isArray(data[attributeType])) {
      randomObject[attributeType] = generateRandomAttribute(
        data[attributeType]
      ); // Asigna un atributo aleatorio del arreglo de opciones
    } else if (
      !Array.isArray(data[attributeType]) &&
      Object.prototype.hasOwnProperty.call(data[attributeType], "maximo")
    ) {
      // Genera un número aleatorio dentro del rango especificado para el atributo
      randomObject[attributeType] = Math.floor(
        Math.random() *
        (data[attributeType].maximo - data[attributeType].minimo) +
        data[attributeType].minimo
      );
    } else if (typeof data[attributeType] === "number") {
      randomObject[attributeType] = data[attributeType]; // Asigna un valor numérico especificado
    } else {
      randomObject[attributeType] = assignAttributeByType(
        data,
        randomObject,
        attributeType
      ); // Asigna un atributo basado en el tipo de objeto
    }
  }

  return randomObject;
}

/**
 * Asigna un atributo aleatorio de un conjunto de opciones, basado en el tipo de objeto.
 *
 * @param {Object} data - Un objeto que contiene las opciones de atributo por tipo.
 * @param {Object} object - El objeto al que se le asignará el atributo.
 * @param {string} attributeOptions - El tipo de atributo que se debe asignar.
 * @returns {*} - Un atributo aleatorio del tipo especificado.
 */
export function assignAttributeByType(data, object, attributeOptions) {
  for (const option in data[attributeOptions]) {
    if (object["tipo"] === option) {
      return data[attributeOptions][option][
        Math.floor(Math.random() * data[attributeOptions][option].length)
      ]; // Devuelve un atributo aleatorio del tipo especificado
    }
  }
}

/**
 * Genera un atributo aleatorio a partir de un conjunto de opciones.
 *
 * @param {Array} data - Un arreglo que contiene las opciones para el atributo.
 * @returns {*} - Un atributo aleatorio seleccionado del arreglo de opciones.
 */
export function generateRandomAttribute(data) {
  return data[Math.floor(Math.random() * data.length)]; 
}

/**
 * Genera un identificador único basado en el sello de tiempo en milisegundos
 * y un número aleatorio entre 0 y 999.
 *
 * @returns {string} - Un identificador único.
 */
export function generateUniqueId() {
  const timestamp = new Date().getTime();  
  const randomValue = Math.floor(Math.random() * 1000);  
  const uniqueId = `${timestamp}${randomValue}`;  
  return uniqueId;
}
