import { useState } from "react";

/**
 * Hook que gestiona la popularidad de un automóvil.
 * @param {number} popularidadActual Popularidad actual del automóvil. 
 * @returns {Array} Array con la popularidad y la función para aumentarla en 1.
 */
export default function usePopularidad(popularidadActual) {
    const [popularidad, setPopularidad] = useState(popularidadActual);

    const aumentar = () => { setPopularidad(popularidad + 1); };

    return [popularidad, aumentar];
}
